package tk.hoopy.aw.space;

public class SpiralGalaxy extends Galaxy {
	
	private final int ArmNumber = 8;//Don't raise beyond 8, due to performance issues.

	public void GenSectors() {		//Special gen algorithm for SpiralGalaxy
		Cache.ClearAll();
		for(int x=0;x<ArmNumber;x++){
			long tempSeed = SeedID+x*this.ArmNumber;
			double Multiplier = x*(Math.PI/ArmNumber)*2;
			for(double i=-Multiplier;i<4*Math.PI;i+=0.01){
				long FinalSeed = (long)(tempSeed+i);
				double tempR = Math.pow(Math.E, i*0.306349);
				double TempX = tempR*Math.cos(i+Multiplier);
				double TempY = tempR*Math.sin(i+Multiplier);
				double VectX = Math.pow(Math.E, (i+.01)*0.306349)*Math.cos(i+.01+Multiplier);
				double VectY = Math.pow(Math.E, (i+.01)*0.306349)*Math.sin(i+.01+Multiplier);
				if(i>=0){
					Cache.AddSector(TempX, TempY, FinalSeed, 4*Math.PI, i, VectX, VectY);
				}
			}
		}
		Cache.GenStars();
	}

}
