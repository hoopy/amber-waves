package tk.hoopy.aw.space;

import java.util.ArrayList;

import org.lwjgl.opengl.GL11;

public abstract class Galaxy {
	
	private int List;			//Stored for drawing to a list
	protected long SeedID = 42;	//Seed used for recreating random galaxies
	protected StarCache Cache = new StarCache();	//Holds sectors and stars
	public abstract void GenSectors();	//Abstract to generate sectors and their locations
	
	public void DrawLargeMap(){		//Draw the list
		GL11.glCallList(List);
	}
	
	public void GenLargeMap() {		//Create the draw list
		
		List = GL11.glGenLists(1);
		GL11.glNewList(List, GL11.GL_COMPILE);
		Cache.DrawStarLimited();
		GL11.glEndList();
		
	}
	
	protected class StarCache{		//Holds sectors and stars
		private ArrayList<Sector> SectorCache = new ArrayList<Sector>();			//Holds sectors
		private ArrayList<double[]> StarCache = new ArrayList<double[]>();			//Holds all stars - drawn stars
		private ArrayList<double[]> SmallStarCache = new ArrayList<double[]>();		//Holds drawn stars
		
		public void GenStars(){
			for(int i=0;i<SectorCache.size();i++){
				double[][] tempStars = SectorCache.get(i).GenStars();
				for(int x=0;x<tempStars.length;x++){
					StarCache.add(tempStars[x]);
					if(Math.random()*Math.random()*Math.random()*Math.random()*Math.random()*Math.random()<.00006){
						SmallStarCache.add(tempStars[x]);
					}
				}
			}
		}
		
		private void DrawStar(double X, double Y, double Z, double C){	//Draw a point
			GL11.glColor3d(C,C,C);
			GL11.glPointSize(2.3f);
			GL11.glBegin(GL11.GL_POINTS);
			GL11.glVertex3d(X,Y,Z);
			GL11.glEnd();
		}
		
		public void DrawStarLimited(){
			for(int i=0;i<SmallStarCache.size();i++){
				DrawStar(	SmallStarCache.get(i)[0],SmallStarCache.get(i)[1],SmallStarCache.get(i)[2],SmallStarCache.get(i)[3]);
			}
		}
		
		public void DrawStar(){
			for(int i=0;i<StarCache.size();i++){
				DrawStar(	StarCache.get(i)[0],StarCache.get(i)[1],StarCache.get(i)[2],StarCache.get(i)[3]);
			}
		}
		
		public void AddSector(double X, double Y, long Seed, double Size, double Distance, double VectX, double VectY){
			SectorCache.add(new Sector(X,Y,Seed,Size,Distance,VectX,VectY));
		}
		
		public void ClearAll(){
			SectorCache.clear();
			StarCache.clear();
		}
		
	}

}
