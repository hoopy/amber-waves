package tk.hoopy.aw.space;

import java.util.ArrayList;
import java.util.Random;

public class Sector {
	
	private long GenSeed;
	private double Distance, GSize;
	private double LocalX, LocalY, Slope;
	
	public Sector(	double X, double Y, long Seed, double Size, double DistanceID,
					double VectX, double VectY){
		Distance = DistanceID;
		GSize = Size;
		GenSeed = Seed;
		LocalX = X;
		LocalY = Y;
		Slope = (VectY-Y)/(VectX-X);
	}
	
	public double GetX(){
		return LocalX;
	}
	
	public double GetY(){
		return LocalY;
	}
	
	public double[][] GenStars(){			//Generate stars based on Sector center and reference point (vector)
		ArrayList<double[]> ordList = new ArrayList<double[]>();
		Random r = new Random();
		r.setSeed((long)GenSeed);
		for(int i=0;i<100;i++){
			double S = (r.nextInt(10000)/1000-5);
			double X = (S*Slope)/Slope+LocalX+(r.nextInt(100)-50)/15;
			double Y = (Math.pow(Slope, -1)*S)/Math.pow(Slope, -1)+LocalY+(r.nextInt(100)-50)/15;
			double Z = r.nextInt(10000)/2000-2.5;
			if((GSize/Distance-r.nextInt(3)) > 0){
				double C = 1-Math.abs(S+5)/10;
				ordList.add(new double[]{X,Y,Z,C});
			}
		}
		double[][] Table = new double[ordList.size()][4];
		for(int i=0;i<ordList.size();i++){
			Table[i] = ordList.get(i);
		}
		return Table;
	}
	
}
