package tk.hoopy.aw;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import org.lwjgl.input.*;

import tk.hoopy.aw.display.Camera;
import tk.hoopy.aw.display.DisplayManager;
import tk.hoopy.aw.space.Galaxy;
import tk.hoopy.aw.space.SpiralGalaxy;

public class MainLoop {
	
	private Galaxy spiral = new SpiralGalaxy();		//Create a galaxy for testing purposes

	public void BeginLoop(){
		spiral.GenSectors();		//Generate the sectors for the sample galaxy
		spiral.GenLargeMap();		//Generate the large map drawlist
		
		float x,y,z;	//Holds the rotation coords
		
		while(!Display.isCloseRequested()){
			
			if(Keyboard.isKeyDown(Keyboard.KEY_W)){
				x=1;
			}else if(Keyboard.isKeyDown(Keyboard.KEY_S)){
				x=-1;
			}else{
				x=0;
			}
			
			if(Keyboard.isKeyDown(Keyboard.KEY_A)){
				y=-1;
			}else if(Keyboard.isKeyDown(Keyboard.KEY_D)){
				y=1;
			}else{
				y=0;
			}
			
			if(Keyboard.isKeyDown(Keyboard.KEY_Q)){
				z=1;
			}else if(Keyboard.isKeyDown(Keyboard.KEY_E)){
				z=-1;
			}else{
				z=0;
			}
			
			Camera.Rotate(x, y, z);					//Rotate the camera with the rotate coords
			if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)){
				Camera.Translate(0, 0, 1);
			}
			
			Vector4f v = Camera.GetAxisAngle();		//Return an AxisAngle vector
			Vector3f p = Camera.GetPosition();		//Return a position vector
			
			GL11.glRotatef(v.w, v.x, v.y, v.z);		//Rotate the screen using the AxisAngle vector
			GL11.glTranslatef(p.x, p.y, p.z);		//Translate the screen using the position vector
			
			spiral.DrawLargeMap();					//Draw the sample galaxy
			DisplayManager.RefreshDisplay();		//Clear screen and sync
			
		}
	}
}
