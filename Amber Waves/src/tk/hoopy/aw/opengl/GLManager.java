package tk.hoopy.aw.opengl;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import tk.hoopy.aw.display.DisplayManager;

public class GLManager {
	
	public static void InitializeGL(){
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glClearColor(0, 0, 0, 1);
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GLU.gluPerspective(	90,
							Display.getWidth()/Display.getHeight(),
							0.1f,
							1000);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glLoadIdentity();
		DisplayManager.RefreshDisplay();
	}
	
}
