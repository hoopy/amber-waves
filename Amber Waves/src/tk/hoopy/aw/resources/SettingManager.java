package tk.hoopy.aw.resources;

import java.io.File;
import java.io.IOException;

import tk.hoopy.aw.resources.TxtManager;

public class SettingManager {
	
	private static boolean isFullscreen, vSync;
	private static int Width,Height;
	
	public static boolean IsFullscreen(){
		return isFullscreen;
	}
	
	public static boolean IsVSync(){
		return vSync;
	}
	public static int GetWidth(){
		return Width;
	}
	
	public static int GetHeight(){
		return Height;
	}
	
	public static void LoadSettings() throws IOException{
		File SettingsFile = new File("Settings.conf");
		TxtManager txt = new TxtManager(SettingsFile);
		if(!SettingsFile.exists()){
			SettingsFile.createNewFile();
			BuildSettings(txt);
		}
		String[] S = txt.ReadFrom();
		for(int i=0;i<S.length;i++){
			String[] temp = S[i].split(":");
			if(temp[0].equals("fullscreen")){
				if(Integer.parseInt(temp[1])==0){
					isFullscreen = false;
				}else{
					isFullscreen = true;
				}
			}else if(temp[0].equals("width")){
				Width = Integer.parseInt(temp[1]);
			}else if(temp[0].equals("height")){
				Height = Integer.parseInt(temp[1]);
			}else if(temp[0].equals("vsync")){
				if(Integer.parseInt(temp[1])==0){
					vSync = false;
				}else{
					vSync = true;
				}
			}
		}
	}
	
	private static void BuildSettings(TxtManager F) throws IOException{
		String S[] = {
					"fullscreen:0",
					"width:800",
					"height:600",
					"vsync:0"
		};
		F.WriteTo(S);
	}
	
}
