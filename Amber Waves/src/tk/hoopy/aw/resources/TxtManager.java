package tk.hoopy.aw.resources;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class TxtManager {
	
	private File F;
	
	public TxtManager(File f){
		F = f;
	}
	
	public void WriteTo(String[] S) throws IOException{
		FileWriter fw = new FileWriter(F);
		for(int i=0;i<S.length;i++){
			fw.write(S[i]+"\n");
		}
		fw.close();
	}
	
	public String[] ReadFrom() throws IOException{
		FileReader fr = new FileReader(F);
		BufferedReader br = new BufferedReader(fr);
		ArrayList<String> temp = new ArrayList<String>();
		String strLine;
		while((strLine = br.readLine()) != null){
			temp.add(strLine);
		}
		br.close();
		fr.close();
		String[] S = new String[temp.size()];
		for(int i=0;i<temp.size();i++){
			S[i] = temp.get(i);
		}
		return S;
	}
	
}
