package tk.hoopy.aw;

import java.io.IOException;

import org.lwjgl.LWJGLException;

import tk.hoopy.aw.opengl.GLManager;
import tk.hoopy.aw.resources.SettingManager;
import tk.hoopy.aw.display.DisplayManager;

public class Starter {
	
	Starter(){
		
	}
	
	public static void main(String[] args) {
		
		Starter S = new Starter();
		S.StartGame();
		
	}
	
	public void StartGame(){
        System.setProperty("org.lwjgl.librarypath", "C:\\users\\Jesse\\ideaprojects\\amber-waves\\Amber Waves\\lwjgl\\native\\windows");
		try {
			SettingManager.LoadSettings();			//Load settings from file
			DisplayManager.CreateDisplay();			//Intialize and create Display
			GLManager.InitializeGL();				//Initialize GL settings
		} catch (IOException | LWJGLException e) {
			e.printStackTrace();
			System.exit(1);
		}
		MainLoop loop = new MainLoop();
		loop.BeginLoop();							//Begin game loop
	}
	
}
