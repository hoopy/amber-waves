package tk.hoopy.aw.display;

import org.lwjgl.util.vector.Matrix3f;
import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

public final class Camera {
	
	private static Quaternion keyQuat = new Quaternion();		//Quaternion made from new movements
	private static Quaternion camQuat = new Quaternion();		//Final quaternion
	private static Quaternion tempQuat = new Quaternion();		//temporary, used for math
	private static Vector3f Pos = new Vector3f(0,0,-100);
	private static Vector3f tempPos = new Vector3f(0,0,0);
	
	public static void Rotate(float X, float Y, float Z){
		double	ex, ey, ez;								//Temp half euler angles
		double	cr, cp, cy, sr, sp, sy, cpcy, spsy;		//Temp vars in roll,pitch yaw
		ex = Math.toRadians(X) / 2.0;					//Convert to rads and half them
		ey = Math.toRadians(Y) / 2.0;
		ez = Math.toRadians(Z) / 2.0;
		cr = Math.cos(ex);
		cp = Math.cos(ey);
		cy = Math.cos(ez);
		sr = Math.sin(ex);
		sp = Math.sin(ey);
		sy = Math.sin(ez);
		cpcy = cp * cy;
		spsy = sp * sy;
		keyQuat.w = (float) (cr * cpcy + sr * spsy);
		keyQuat.x = (float) (sr * cpcy - cr * spsy);
		keyQuat.y = (float) (cr * sp * cy + sr * cp * sy);
		keyQuat.z = (float) (cr * cp * sy - sr * sp * cy);		//Assign values to new Quaternion
		
		tempQuat = camQuat;
		Quaternion.mul(keyQuat, tempQuat, camQuat);		//Multiply quaternions for new quaternion
	}
	
	public static void Translate(float X, float Y, float Z){
		Vector3f vect = new Vector3f(X,Y,Z);
		Matrix3f tempMat = new Matrix3f();
		Vector3f newVect = new Vector3f();
		
		tempMat.m00 = (float) (1-2*Math.pow(camQuat.y,2)-2*Math.pow(camQuat.z,2));
		tempMat.m10 = 2*(camQuat.x*camQuat.y+camQuat.w*camQuat.z);
		tempMat.m20 = 2*(camQuat.x*camQuat.z-camQuat.w*camQuat.y);
		tempMat.m01 = 2*(camQuat.x*camQuat.y-camQuat.w*camQuat.z);
		tempMat.m11 = (float) (1-2*Math.pow(camQuat.x,2)-2*Math.pow(camQuat.z,2));
		tempMat.m21 = 2*(camQuat.y*camQuat.z+camQuat.w*camQuat.x);
		tempMat.m02 = 2*(camQuat.x*camQuat.z+camQuat.w*camQuat.y);
		tempMat.m12 = 2*(camQuat.y*camQuat.z-camQuat.w*camQuat.x);
		tempMat.m22 = (float) (1-2*Math.pow(camQuat.x,2)-2*Math.pow(camQuat.y,2));
		
		Matrix3f.transform(tempMat, vect, newVect);
		tempPos = Pos;
		Vector3f.add(tempPos, newVect, Pos);
	}
	
	public static Vector4f GetAxisAngle(){
		double	temp_angle;		//Temp angle
		double	scale;			//Temp scale for division
		Vector4f v = new Vector4f();	//Vector to hold AxisAngles
		temp_angle = Math.acos(camQuat.w);
		scale = (float)Math.sin(temp_angle);
		
		if(scale==0){
			v.set(0, 0, 1, 0);
		}else{
			v.set(	(float)(camQuat.x/scale),
					(float)(camQuat.y/scale),
					(float)(camQuat.z/scale),
					(float)Math.toDegrees(temp_angle * 2.0));
		}
		return v;
	}
	
	public static Vector3f GetPosition(){
		return Pos;
	}
	
	
}
