package tk.hoopy.aw.display;

import tk.hoopy.aw.resources.SettingManager;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

public class DisplayManager {
	
	private static double Delta = System.currentTimeMillis();	//Delta time
	
	public static void RefreshDisplay(){
		Display.update();													//Update display
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);	//Clear screen
		GL11.glLoadIdentity();	//Reset matrix
		Display.sync(45);		//Sync display
		
		Display.setTitle( "[FPS: "+(int)Math.pow(((System.currentTimeMillis()-Delta)/1000),-1)+"] Amber Waves" );
		Delta = System.currentTimeMillis();
		
	}
	
	public static void CreateDisplay() throws LWJGLException{
		if(Display.isCreated()){
			Display.destroy();
		}
		if(SettingManager.IsFullscreen()){
			Display.setFullscreen(SettingManager.IsFullscreen());
			Display.setVSyncEnabled(SettingManager.IsVSync());
		}else{
			Display.setDisplayMode(new DisplayMode(SettingManager.GetWidth(),SettingManager.GetHeight()));
		}
		Display.setResizable(false);
		Display.setTitle("Amber Waves");
		Display.create();
	}
	
}
